
# coding: utf-8



from scipy import misc
import numpy as np
from PIL import Image
from resizeimage import resizeimage
import matplotlib.pyplot as plt
import cv2
# import imageio
import  os
import image_slicer
import statistics

np.set_printoptions(threshold=np.nan)

# Tamaño imagen

def tratarImagenBN(nArray):
    for i in np.transpose(np.where(nArray > 150)):
        nArray[i[0]][i[1]] = 255

    for i in np.transpose(np.where(nArray <= 150)):
        nArray[i[0]][i[1]] = 0

    return nArray

def calculaPorcentajeNegros(nArray):
    blancos = 0
    negros = 0
    for i in np.transpose(np.where(nArray > 150)):
        blancos = blancos+1

    for i in np.transpose(np.where(nArray <= 150)):
        negros = negros + 1

    return (negros/(negros + blancos))

def trataImagen(imagenName, clase):
    nArray = misc.imread('out/'+ imagenName +'.png',True)
    nArray = tratarImagenBN(nArray)

    im = Image.fromarray(nArray)
    misc.imsave(imagenName + '_grey.png',nArray)


    ### load input image and convert it to grayscale
    img = cv2.imread(imagenName + "_grey.png")
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #### extract all contours
    _, contours, _  = cv2.findContours(gray.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    #### create one bounding box for every contour found
    bb_list = []
    for c in contours:
        bb = cv2.boundingRect(c)
        # save all boxes except the one that has the exact dimensions of the image (x, y, width, height)
        if (bb[0] == 0 and bb[1] == 0 and bb[2] == img.shape[1] and bb[3] == img.shape[0]):
            continue
        bb_list.append(bb)

    # debug: draw boxes
    img_boxes = img.copy()
    img_cajaGuay = img.copy()
    j = 0
    for bb in bb_list:
        x,y,w,h = bb
        i = w
        if(i>j):
            j=i
            rectangulo = bb
        cv2.rectangle(img_boxes, (x, y), (x+w, y+h), (0, 0, 255), 2)
    cv2.imwrite(imagenName + "_boxes.jpg", img_boxes)

    x,y,w,h = rectangulo
    cv2.rectangle(img_cajaGuay, (x, y), (x+w, y+h), (0, 0, 255), 2)

    #### sort bounding boxes by the X value: first item is the left-most box
    bb_list.sort(key=lambda x:x[0])

    # crop to the roi
    crop_img = img[y:y+h, x:x+w]
    cv2.imwrite(imagenName + "_crop.jpg", crop_img)

    # codigo de error
    if w < 20 or h < 20:
        return -1


    # Reducimos el tam de la imagen
    imgwidth = int(w*2/3)
    imgheight = int(h*2/3)

    with open (imagenName + "_crop.jpg", "r+b") as f:
         with Image.open(f) as image:
            cover = resizeimage.resize_cover(image, [imgwidth, imgheight])
            cover.save(imagenName + '_salida.jpeg', image.format)


    nArrayNuevo = misc.imread(imagenName + '_salida.jpeg',True)

    ######################### optimizar ####################################
    image_slicer.slice(imagenName + "_crop.jpg", 9)


    # Division en 9
    nArray1 = misc.imread(imagenName + '_crop_01_01.png',True)
    nArray1 = tratarImagenBN(nArray1)
    nArray2 = misc.imread(imagenName + '_crop_01_02.png',True)
    nArray2 = tratarImagenBN(nArray2)
    nArray3 = misc.imread(imagenName + '_crop_01_03.png',True)
    nArray3 = tratarImagenBN(nArray3)

    nArray4 = misc.imread(imagenName + '_crop_02_01.png',True)
    nArray4 = tratarImagenBN(nArray4)
    nArray5 = misc.imread(imagenName + '_crop_02_02.png',True)
    nArray5 = tratarImagenBN(nArray5)
    nArray6 = misc.imread(imagenName + '_crop_02_03.png',True)
    nArray6 = tratarImagenBN(nArray6)

    nArray7 = misc.imread(imagenName + '_crop_03_01.png',True)
    nArray7 = tratarImagenBN(nArray7)
    nArray8 = misc.imread(imagenName + '_crop_03_02.png',True)
    nArray8 = tratarImagenBN(nArray8)
    nArray9 = misc.imread(imagenName + '_crop_03_03.png',True)
    nArray9 = tratarImagenBN(nArray9)

    listaTemp = []
    listaTemp.append(calculaPorcentajeNegros(nArray1))
    listaTemp.append(calculaPorcentajeNegros(nArray2))
    listaTemp.append(calculaPorcentajeNegros(nArray3))
    listaTemp.append(calculaPorcentajeNegros(nArray4))
    listaTemp.append(calculaPorcentajeNegros(nArray5))
    listaTemp.append(calculaPorcentajeNegros(nArray6))
    listaTemp.append(calculaPorcentajeNegros(nArray7))
    listaTemp.append(calculaPorcentajeNegros(nArray8))
    listaTemp.append(calculaPorcentajeNegros(nArray9))
    listaTemp.append(clase)

    ######################### optimizar ####################################

    import os
    dir_name = "/Users/lucasruiz/Desktop/FAA_PROYECTO"
    test = os.listdir(dir_name)

    for item in test:
        if (item.endswith(".jpeg") or item.endswith(".png") or item.endswith(".jpg")):
            os.remove(os.path.join(dir_name, item))

    return listaTemp
##################################################
def generaDataSet(letra, letraNum, fSalida):
    lista = []
    tocaBorde = 0
    for i in range(0, 131):
        num = i*10 + letraNum
        if(num<=9):
            num = '0000' + str(num)
        elif(num<=99):
            num = '000' + str(num)
        elif(num<=999):
            num = '00' + str(num)
        elif(num<=9999):
            num = '0' + str(num)
        elif(num<=99999):
            num = str(num)
        imagenNameTmp = 'l' + num + letra
        listaTmp = trataImagen(imagenNameTmp, letraNum)
        if listaTmp == -1:
            tocaBorde = tocaBorde +1
        else:
            for att in listaTmp:
                fSalida.write(str(att) + " ")
            fSalida.write("\n")
            lista.append(listaTmp)

    w, h = len(lista), len(lista[0])

    Matrix = [[0 for x in range(w)] for y in range(h)]
    i = 0
    for object in lista:
        for j in range(len(object)-1):
            Matrix[j][i] = object[j]
        i += 1

    medias = []
    for i in range(len(Matrix)):
        medias.append(statistics.mean(Matrix[i]))
    print("La imagen de tipo: " + letra + " ha tenido " + str(tocaBorde) + " bordes tocados")
    return medias

# Comienza el main

fSalida = open("datasetTmp.txt", "w+")

listaMedias = []
letraNum = 0
letra = '_A'
generaDataSet(letra, letraNum, fSalida)

letraNum = 1
letra = '_B'
generaDataSet(letra, letraNum, fSalida)

letraNum = 2
letra = '_C'
generaDataSet(letra, letraNum, fSalida)

letraNum = 3
letra = '_D'
generaDataSet(letra, letraNum, fSalida)

letraNum = 4
letra = '_E'
generaDataSet(letra, letraNum, fSalida)

letraNum = 5
letra = '_F'
generaDataSet(letra, letraNum, fSalida)

letraNum = 6
letra = '_G'
generaDataSet(letra, letraNum, fSalida)

letraNum = 7
letra = '_H'
generaDataSet(letra, letraNum, fSalida)

letraNum = 8
letra = '_I'
generaDataSet(letra, letraNum, fSalida)

letraNum = 9
letra = '_J'
generaDataSet(letra, letraNum, fSalida)

fSalida.close()



