import numpy as np
import random
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn import neighbors, datasets

#https://scikit-learn.org/stable/auto_examples/neighbors/plot_classification.html#sphx-glr-auto-examples-neighbors-plot-classification-py


fich = ["./outContinuo/dataset_4atts.txt","./outContinuo/dataset_9atts.txt","./outContinuo/dataset_16atts.txt","./outContinuo/dataset_20atts.txt"]

puntuacion = []
for fil in fich:
    print("\n\nFichero", fil)
    array = np.loadtxt(fil)
    random.shuffle(array)

    train=int(0.3*array.shape[0])
    atributos_X_train = array[:-train]
    atributos_X_test = array[-train:]

    atributos_y_train = array[:-train,-1]
    atributos_y_test = array[-train:,-1]


    scores = []
    scoresMax = []

    for k in range(1,200,10):
        sco = []
        # print("\nProbando con ", k, "vecinos")
        for weights in ['uniform', 'distance']:
            # we create an instance of Neighbours Classifier and fit the data.
            clfSklearn = neighbors.KNeighborsClassifier(k, weights=weights)
            clfSklearn.fit(atributos_X_train, atributos_y_train)

            Z = clfSklearn.predict(atributos_X_test)
            escore = clfSklearn.score(atributos_X_test,atributos_y_test)
            # print("Score con distancia", weights, ": ", escore)
            sco.append(escore)
        scores.append(sco)
        scoresMax.append(np.max(sco))

    # print("\nScores:\n", scores)
    # print("ScoresMax:\n",scoresMax)
    print("ScoresMax")
    for enum,k in enumerate(range(1,500,50)):
        print("Con ", k, "vecinos-->\t", scoresMax[enum])
    
    puntuacion.append(scoresMax)
