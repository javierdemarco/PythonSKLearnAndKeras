
# Comenzamos dividiendo las imagenes
import os, shutil

# Basado en sistema operativo, Mac OS /, Linux \, Windows \\
slash =  "\"
baseDir = "/Users/lucasruiz/Desktop/FAA_PROYECTO"
salida = "salida"
os.chdir(baseDir)
os.mkdir(salida)

# Dividimos en los ficheros de las imagenes
for f in os.listdir("out"):
    folderName = f[-5:-4]
    folderPath = salida + slash + folderName
    if not os.path.exists(os.path.join(baseDir,folderPath)):
        os.mkdir(os.path.join(baseDir,folderPath))
        shutil.copy(os.path.join('out', f), os.path.join(baseDir,folderPath))
    else:
        shutil.copy(os.path.join('out', f), os.path.join(baseDir,folderPath))

# Obtenemos la lista de ficheros
from os import walk
for (dirpath, dirnames, filenames) in walk(os.path.join(baseDir,salida)):
    if dirnames != []:
        listaDirs = dirnames

# Primera letra para contar el número de files
letra = listaDirs[0]

# Se establece la variable DIR que será usada más adelante
DIR = baseDir + slash + salida + slash + letra

nFiles = (len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]))
print("El total de archivos es: " + str(nFiles))

# Porcentaje
print("Introduzca el porcentaje deseado de test: ")
porcentaje = input()
porcentaje = int(porcentaje)
nTest = round((nFiles * porcentaje) / 100)

print(nTest)

# Creo el directorio para test una vez
dirTest = baseDir + slash + salida + slash + "Testing"
dirTrain = baseDir + slash + salida + slash + "Training"
os.mkdir(dirTest)
os.mkdir(dirTrain)

for letra in listaDirs:
    # Intentando poner en Training y Test
    import random
    from os import listdir
    from os.path import isfile, join

    # Se establece la variable DIR que será usada más adelante
    DIR = baseDir + slash + salida + slash + letra

    # Creo el directorio para la letra
    dirActu = dirTest + slash + letra
    os.mkdir(dirActu)
    for x in range(nTest):
        # Pillo todos los archivos aquí para que no falle
        onlyFiles = [f for f in listdir(DIR) if isfile(join(DIR, f))]
        file = random.choice(onlyFiles)
        oldPath = DIR + slash + file
        newPath = dirActu + slash + file
        shutil.move(oldPath,newPath)
    newPath = dirTrain + slash + letra
    shutil.move(DIR, newPath)

# Empezamos a clasificar
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense

# Parte 1 - Crando el clasificador
# Inicializamos el clasificador en secuencial
classifier = Sequential()

# Primer paso - Añadir una capa convolucional con unidad lineal rectificada
classifier.add(Conv2D(32, (3, 3), input_shape = (32,32,3), activation = 'relu'))

# Segundo paso - Agrupamos
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Tercer paso - Añadimos una segunda capa, y la agrupamos
classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Cuatro paso - "Aplanamos" la entrada (Flattening)
classifier.add(Flatten())

# Quinto paso - Conexión total
classifier.add(Dense(units = 128, activation = 'relu'))
classifier.add(Dense(units = 10, activation = 'softmax'))

# Compilamos el clasificador
classifier.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])

#######################################################################################################

# Parte 2 - Ajustado las red CNN a las imágenes

from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)



training_set = train_datagen.flow_from_directory(dirTrain,
                                                 target_size = (32, 32),
                                                 batch_size = 32,
                                                 class_mode = 'categorical')

test_set = test_datagen.flow_from_directory(dirTest,
                                            target_size = (32, 32),
                                            batch_size = 32,
                                            class_mode = 'categorical')

# Entrenamos el modelo
classifier.fit_generator(training_set,
                         steps_per_epoch = 500,
                         epochs = 13,
                         validation_data = test_set,
                         validation_steps = 500)


# Parte 3 - Pruebas, haciendo nuevas predicciones

import numpy as np
from keras.preprocessing import image

dirClasifica = baseDir + slash + "clasificando"
onlyFiles = [f for f in listdir(dirClasifica) if isfile(join(dirClasifica, f))]

for imagen in onlyFiles:
    print("La imagen " + imagen + " es clasificada como: ")
    dirImagen = dirClasifica + slash + imagen
    test_image = image.load_img(dirImagen, target_size = (32,32))
    test_image = image.img_to_array(test_image)
    test_image = np.expand_dims(test_image, axis = 0)
    result = classifier.predict(test_image)


    if result[0][0] == 1:
        print('a')
    elif result[0][1] == 1:
        print('b')
    elif result[0][2] == 1:
        print('c')
    elif result[0][3] == 1:
        print('d')
    elif result[0][4] == 1:
        print('e')
    elif result[0][5] == 1:
        print('f')
    elif result[0][6] == 1:
        print('g')
    elif result[0][7] == 1:
        print('h')
    elif result[0][8] == 1:
        print('i')
    elif result[0][9] == 1:
        print('j')
